# GitOps on Kubernetes

## Table of Contents

- [Objective](#objective)
- [Repositories](#repositories)
- [Development](#development)
- [Setup](#setup)
- [Structure](#structure)
- [Multi-tenancy](#multi-tenancy)
- [Secret Management](#secret-management)
- [Automatic image update](#automatic-image-update)
- [Canary deployment](#canary-deployment)

## Objective

This project demonstrate using GitOps approach to bootstrap and manage Kubernetes clusters, with features including:

- Multi-tenancy
- Secret management
- Automatic image update
- Canary deployment

## Repositories

This project contains the following repositories to store Kubernetes manifests for different purposes.

- [Infrastructure](https://gitlab.com/lokflam-demo/gitops/infra)
- [GitOps components in "dev" cluster](https://gitlab.com/lokflam-demo/gitops/gitops-dev)
- [GitOps components in "prod" cluster](https://gitlab.com/lokflam-demo/gitops/gitops-prod)
- [GitOps components in sample tenant "team-a" for "dev" environment](https://gitlab.com/lokflam-demo/gitops/gitops-tenant-team-a-dev)
- [Demo of automatic image update](https://gitlab.com/lokflam-demo/gitops/team-a-demo-image-update)
- [Demo of canary deployment](https://gitlab.com/lokflam-demo/gitops/team-a-demo-istio-canary)

## Development

For testing and development, It's recommended to fork the repositories and modify configurations in the following resources.

### GitRepository

- https://gitlab.com/lokflam-demo/gitops/gitops-dev/-/tree/main/infra/gitrepositories
- https://gitlab.com/lokflam-demo/gitops/gitops-dev/-/blob/main/tenants/team-a.yaml
- https://gitlab.com/lokflam-demo/gitops/gitops-prod/-/tree/main/infra/gitrepositories
- https://gitlab.com/lokflam-demo/gitops/gitops-tenant-team-a-dev/-/blob/main/demo-istio-canary.yaml
- https://gitlab.com/lokflam-demo/gitops/gitops-tenant-team-a-dev/-/blob/main/demo-image-update.yaml

### Secrets

- https://gitlab.com/lokflam-demo/gitops/infra/-/tree/main/cluster/dev
- https://gitlab.com/lokflam-demo/gitops/infra/-/tree/main/cluster/prod

## Setup

This project includes the following components. This section includes the steps for setting up the cluster.

- FluxCD
- kube-prometheus-stack (Prometheus, Grafana, kube-state-metrics, etc)
- Istio
- Kyverno
- Flagger
- Sample tenant (only included in dev cluster)

### Prerequisites

- Kubernetes cluster (e.g. [kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation), GKE, EKS)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- [kustomize](https://kubectl.docs.kubernetes.io/installation/kustomize/)
- [helm](https://helm.sh/docs/intro/install/) (Optional)

### Bootstrap dev cluster

1. (Optional) This project does not include LoadBalancer installation. Since the Istio default setup is using LoadBalancer service, install LoadBalancer or customize the manifests if required. For using kind cluster, you may run:

    ```bash
    # ref: https://kind.sigs.k8s.io/docs/user/loadbalancer/
    helm repo add metallb https://metallb.github.io/metallb
    helm upgrade metallb metallb/metallb \
        --version 0.10.2 -n metallb-system \
        --install --create-namespace \
        --set configInline.address-pools[0].name=default \
        --set configInline.address-pools[0].protocol=layer2 \
        --set configInline.address-pools[0].addresses[0]=<CIDR_RANGE>
    ```

2. Install FluxCD.

    ```bash
    kustomize build https://gitlab.com/lokflam-demo/gitops/infra.git/fluxcd/base | kubectl apply -f -
    ```

3. Create secret for SOPS decryption. This project is using SOPS for secret encryption/decryption. Refer to [Secret Management](#secret-management) for more details.

    ```bash
    kubectl apply -f - <<EOF
    apiVersion: v1
    kind: Secret
    metadata:
        name: sops-age
        namespace: flux-system
    stringData:
        age.agekey: |
            AGE-SECRET-KEY-1YQ4ENMNKNPA7X6J7L3Y4Q9LVP0HQSRSRLWKGS2WDKFQL4GTGRLTQ9L6JNS
    type: Opaque
    EOF
    ```

4. Create secret for Git access. You should generate your own access token for testing and development.

    ```bash
    kubectl apply -f - <<EOF
    apiVersion: v1
    kind: Secret
    metadata:
        name: gitlab-credentials
        namespace: flux-system
    stringData:
        username: gitops
        password: <GITLAB_ACCESS_TOKEN>
    type: Opaque
    EOF
    ```

5. Install GitOps components for dev cluster.

    ```bash
    kustomize build https://gitlab.com/lokflam-demo/gitops/gitops-dev.git | kubectl apply -f -
    ```

6. FluxCD will start to reconcile Kubernetes resources. Use the following command to check the status. The process will require ~10 mins.

    ```bash
    watch -n2 kubectl get kustomizations.kustomize.toolkit.fluxcd.io -A
    ```

7. Access Grafana / Prometheus.

    ```bash
    # Grafana
    # username: admin
    # password: prom-operator
    kubectl port-forward -n monitoring svc/kube-prometheus-stack-grafana 8080:80

    # Prometheus
    kubectl port-forward -n monitoring svc/kube-prometheus-stack-prom-prometheus 9090:9090
    ```

### Bootstrap prod cluster

The steps to bootstrap a prod cluster is nearly the same as dev cluster. Except for the follow steps.

- For step 3, use the follow secret instead of the one for dev.

    ```bash
    kubectl apply -f - <<EOF
    apiVersion: v1
    kind: Secret
    metadata:
        name: sops-age
        namespace: flux-system
    stringData:
        age.agekey: |
            AGE-SECRET-KEY-1D207SMYDA07W5DK9CHQX3YK0KU8QNXZERLRW4MJDRV84EPAXJ43SGYENVL
    type: Opaque
    EOF
    ```

- For step 5, install from the prod repository.

    ```bash
    kustomize build https://gitlab.com/lokflam-demo/gitops/gitops-prod.git | kubectl apply -f -
    ```

## Structure

The structure of the core components is as follow:

### infra repository

Store manifests for infrastructure. It can be separated as multiple repositories.

Each directory in the repository is a component for the cluster, e.g. fluxcd, istio-operator, kyverno, while the `cluster` component stores the cluster scope or cluster specific resources.

```
- cluster
- flagger
- fluxcd
...
```

For each component, the structure is following Kustomize's practice. The `base` directory contains shared configurations. The `dev` and `prod` directories reference the `base` directory, and contain cluster specific configurations.

```
- fluxcd
    - base
    - dev
    - prod
- istio-operator
    - base
    - dev
    - prod
...
```

### gitops-dev repository

Store FluxCD manifests for dev cluster. This project should only contains FluxCD resources, like `GitRepository`, `HelmRepository` and `Kustomization`.

`GitRepository` resources are always referencing the most recent commit of repositories.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/gitops-dev/-/blob/main/infra/gitrepositories/infra.yaml
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: GitRepository
metadata:
  name: infra
  namespace: flux-system
spec:
  url: https://gitlab.com/lokflam-demo/gitops/infra.git
  interval: 1m
  ref:
    # always referencing the latest commit of main branch
    branch: main
```

Each `Kustomization` will represent one component in infra repository, and reference the `dev` directory of the component. These resources also specified their dependencies, and will not reconcile when any of the dependency is unhealthy or missing.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/gitops-dev/-/blob/main/infra/apps/istio-system.yaml
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: istio-system
  namespace: flux-system
spec:
  # referencing the "dev" directory of "istio-system" component
  path: istio-system/dev
  sourceRef:
    kind: GitRepository
    name: infra
  # reconciliation will not start until these dependencies are installed
  dependsOn:
    - name: istio-operator
    - name: monitoring
```

There is a `Kustomization` for gitops-dev repository itself, which references the most recent commit and responsible to update the dev cluster. Any failed health check for this resource means the core components of the cluster is unhealthy. Non-core components should depend on this `Kustomization` resource.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/gitops-dev/-/blob/main/app.yaml
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: gitops
  namespace: flux-system
spec:
  path: ./
  # referencing gitops-dev repo itself
  sourceRef:
    kind: GitRepository
    name: gitops
  # health checks for core components
  healthChecks:
    - apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
      kind: Kustomization
      name: fluxcd
      namespace: flux-system
    - apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
      kind: Kustomization
      name: cluster
      namespace: flux-system
...
```

### gitops-prod repository

Store FluxCD manifests for prod cluster. Separating repositories to store manifests for different cluster / environment can make permission management easier. Therefore, the FluxCD resources in gitops-prod repository has configurations specific for `prod` cluster, while referencing and reusing the configurations in infra repository.

Similar to gitops-dev repository, but all `GitRepository` resources, except for gitops-prod itself, are referencing a specific commit. As a result, the cluster can always using the stable version of manifests.

```yaml
# https://gitlab.com/lokflam-demo/gitops/gitops-prod/-/blob/main/infra/gitrepositories/infra.yaml
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: GitRepository
metadata:
  name: infra
  namespace: flux-system
spec:
  url: https://gitlab.com/lokflam-demo/gitops/infra.git
  interval: 1m
  ref:
    # referencing a commit
    commit: ce7de4e3f881f123655fcd7e50ff629d82a90ce0
```

```yaml
# https://gitlab.com/lokflam-demo/gitops/gitops-prod/-/blob/main/infra/gitrepositories/gitops.yaml
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: GitRepository
metadata:
  name: gitops
  namespace: flux-system
spec:
  url: https://gitlab.com/lokflam-demo/gitops/gitops-prod.git
  interval: 1m
  ref:
    # for gitops-prod repo, still referencing the latest commit
    branch: main

```

## Multi-tenancy

Multi-tenancy of this demo is achieved by the `serviceAccountName` field in `Kustomization` and `HelmRelease` resources, Kubernetes RBAC, and Kyverno policies.

### Tenant repository and namespaces

Each tenant is assigned a repository storing FluxCD resources, similar to `gitops-dev` and `gitops-prod`. In this example, we defined a tenant called "team-a", and this tenant is deploying to "dev" cluster, so we created the [gitops-tenant-team-a-dev](https://gitlab.com/lokflam-demo/gitops/gitops-tenant-team-a-dev) repository. All FluxCD resources can only be deployed in this repository.

Tenant will have a GitOps namespace storing all the FluxCD resources, and multiple project namespaces. These namespaces should be created by admin, so they are included in the infra repository.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/cluster/base/tenants/team-a/gitops/resources.yaml

# Namespace for FluxCD resources
apiVersion: v1
kind: Namespace
metadata:
  labels:
    gitops-tenant-enabled: "true"
  name: gitops-tenant-team-a
```

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/cluster/base/tenants/team-a/demo-dev/resources.yaml

# Namespace for "demo" project "dev" environment
apiVersion: v1
kind: Namespace
metadata:
  name: team-a-demo-dev
  labels:
    istio-injection: enabled
```

### RBAC

Tenant user will use a service account called "reconciler" to deploy their resources using FluxCD, so we should assign appropriated RBAC rules to it.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/cluster/base/tenants/team-a/gitops/resources.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    tenant: team-a
  name: reconciler
  namespace: gitops-tenant-team-a
```

RBAC for allowing SA to deploy FluxCD resources and GitOps related secrets in the GitOps namespace.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/cluster/base/tenants/common/rbac.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: gitops-tenant
rules:
  - apiGroups:
      - source.toolkit.fluxcd.io
      - kustomize.toolkit.fluxcd.io
      - helm.toolkit.fluxcd.io
      - notification.toolkit.fluxcd.io
      - image.toolkit.fluxcd.io
    resources: ["*"]
    verbs: ["*"]
  - apiGroups:
      - ""
    resources:
      - secrets
    verbs: ["*"]
---
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/cluster/base/tenants/team-a/gitops/resources.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: reconciler
  namespace: gitops-tenant-team-a
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: gitops-tenant
subjects:
  - kind: ServiceAccount
    name: reconciler
    namespace: gitops-tenant-team-a
```

RBAC for allowing SA to deploy common resources in project namespace.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/cluster/base/tenants/common/rbac.yaml

# allow to create flagger resources in addition to those in "admin" ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: flagger-admin
  labels:
    rbac.authorization.k8s.io/aggregate-to-admin: "true"
rules:
  - apiGroups:
      - flagger.app
    resources: ["*"]
    verbs: ["*"]
---
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/cluster/base/tenants/team-a/gitops/resources.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: reconciler
  namespace: team-a-demo-dev
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: admin
subjects:
  - kind: ServiceAccount
    name: reconciler
    namespace: gitops-tenant-team-a
```

### Kyverno policies

This project uses Kyverno to enforce policies in order to achieve the multi-tenancy model.

#### Restrict service account

Since `serviceAccountName` is a non-restricted field. We need policy to enforce tenant user to use the "reconciler" SA. If not, they can deploy resources in any namespaces.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/kyverno-policies/base/enforce-flux-multi-tenancy.yaml
apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: enforce-flux-multi-tenancy
spec:
  validationFailureAction: enforce
  background: false
  rules:
    - name: restrict-serviceaccount
      match:
        resources:
          kinds:
            - Kustomization
            - HelmRelease
      exclude:
        resources:
          namespaces:
            - flux-system
      validate:
        message: "ServiceAccountName must be \"reconciler\"."
        # the "serviceAccountName" field must be set, and the value must be "reconciler"
        pattern:
          spec:
            serviceAccountName: reconciler
...
```

#### Restrict GitRepository namespace

Since the current version of FluxCD is allowing `Kustomization` resources to use any `GitRepository` resources existing in the cluster, we cannot let tenant user to reference `GitRepository` in another namespace.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/kyverno-policies/base/enforce-flux-multi-tenancy.yaml
apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: enforce-flux-multi-tenancy
spec:
  validationFailureAction: enforce
  background: false
  rules:
    ...
    - name: restrict-local-source
      match:
        resources:
          kinds:
            - Kustomization
      exclude:
        resources:
          namespaces:
            - flux-system
      validate:
        message: "Referencing source from another namespace is not allowed."
        # spec.sourceRef.namespace must not be set
        pattern:
          spec:
            =(sourceRef):
              X(namespace): "*?"

```

#### Synchronize GitOps related secrets to tenant namespace

Some secrets should be sync to tenant namespace for different purposes. This example policy synchronize the `sops-age` secret, which is used for decrypting secrets in manifests, to the GitOps namespace of any tenants.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/kyverno-policies/base/sync-gitops-secrets.yaml
apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: sync-gitops-secrets
spec:
  validationFailureAction: enforce
  rules:
    - name: sync-sops-secret
      match:
        resources:
          kinds:
            - Namespace
          selector:
            matchLabels:
              gitops-tenant-enabled: "true"
      generate:
        synchronize: true
        apiVersion: v1
        kind: Secret
        name: sops-age
        namespace: "{{request.object.metadata.name}}"
        clone:
          namespace: flux-system
          name: sops-age
```

## Secret Management

There are many options for GitOps's secret management, like [sealed-secret](https://github.com/bitnami-labs/sealed-secrets) and [kubernetes-external-secrets](https://github.com/external-secrets/kubernetes-external-secrets). This project is using [SOPS](https://github.com/mozilla/sops) for secret encryption / decryption, and the encrypted secrets will be stored in Git.

SOPS requires a command line tool for operations, refer to the [document](https://github.com/mozilla/sops#stable-release) for installation guide.

### Encryption

Encryption rules are configured in `.sops.yaml` file.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/.sops.yaml
creation_rules:
  - path_regex: .+\.dev\.sops\..+
    encrypted_regex: ^(data|stringData)$
    age: age1puucyr43t9264f9rexyvn3rxjy6gsadxkqgzjknvs02r5j5uappqjwsh37,age16whfuy2q62qla5xwuuw8fsjy9tcvle6m4t9766f2hykc5z9hxfss2l604g
  - path_regex: .+\.prod\.sops\..+
    encrypted_regex: ^(data|stringData)$
    age: age1puucyr43t9264f9rexyvn3rxjy6gsadxkqgzjknvs02r5j5uappqjwsh37,age1f0xcpk4xc3ucxkwlul4hg9uq5c54xfrq4kr8eass2h9z3ypj0fdqlc4wdh
  - path_regex: .+\.gcp\.sops\..+
    encrypted_regex: ^(data|stringData)$
    gcp_kms: projects/lokflam-poc-1-1/locations/global/keyRings/lokflam-poc-1-1-test/cryptoKeys/key1
    age: age1puucyr43t9264f9rexyvn3rxjy6gsadxkqgzjknvs02r5j5uappqjwsh37
```

Note that:

- Method of encryption is depended on file name, file `xxx.dev.sops.yaml` will be encrypted by the key of dev cluster, while file `xxx.prod.sops.yaml` will be encrypted by the key of prod cluster, and file `xxx.gcp.sops.yaml` will be encrypted by GCP KMS service, etc.
- No matter of which encryption method, data will also be encrypted by the root key, as a backup plan.
- Only the `data` and `stringData` field will be encrypted
- For non-KMS encryption methods, files are encrypted using [age](https://github.com/FiloSottile/age).

### Decryption

Decryption will be done by FluxCD controller. Secrets will be automatically decrypted before applying. To achieve it, the following configurations are done.

#### Age

To decrypt age encrypted secrets, a secret containing the age key should be specified in `Kustomization` resources.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/cluster/dev/sops-age.dev.sops.yaml
kind: Secret
metadata:
    name: sops-age
    namespace: flux-system
stringData:
    age.agekey: |
        AGE-SECRET-KEY-1YQ4ENMNKNPA7X6J7L3Y4Q9LVP0HQSRSRLWKGS2WDKFQL4GTGRLTQ9L6JNS
type: Opaque
```

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/gitops-dev/-/blob/main/app.yaml
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: gitops
  namespace: flux-system
spec:
  ...
  decryption:
    provider: sops
    secretRef:
      name: sops-age
```

#### GCP KMS

For GCP KMS, no secrets have to be specified, instead, link the GCP service account with FluxCD controller service account.

```hcl
# Example for using Terraform to link GCP and GKE service account

resource "google_service_account" "default" {
  account_id = "sops-test"
}

resource "google_kms_key_ring_iam_binding" "default" {
  key_ring_id = google_kms_key_ring.default.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${google_service_account.default.email}",
  ]
}

resource "google_service_account_iam_binding" "flux" {
  service_account_id = google_service_account.default.id
  role        = "roles/iam.workloadIdentityUser"

  members = [
    "serviceAccount:${data.google_client_config.current.project}.svc.id.goog[flux-system/kustomize-controller]",
  ]
}
```

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/infra/-/blob/main/fluxcd/gcp/kustomization.yaml

# add GCP SA annotation to FluxCD controller SA
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ../base
patches:
  - patch: |
      - op: replace
        path: /metadata/annotations/iam.gke.io~1gcp-service-account
        value: sops-test@lokflam-poc-1-1.iam.gserviceaccount.com
    target:
      kind: ServiceAccount
      name: kustomize-controller
```

## Automatic image update

FluxCD can provide ability to update image in manifest automatically, depending on the metadata in image repository, meaning manifests can be updated when a new version of image is pushed.

`image.toolkit.fluxcd.io` API group handles operations related to image update. `ImageRepository` defines image to be scanned and the accessibility to the registry. `ImagePolicy` defines rule for selecting image tag. `ImageUpdateAutomation` defines where to commit the update and the commit message of the update.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/gitops-tenant-team-a-dev/-/blob/main/demo-image-update.yaml
apiVersion: image.toolkit.fluxcd.io/v1beta1
kind: ImageRepository
metadata:
  name: demo-image
spec:
  interval: 1m
  image: docker.io/lokllf/gitops-demo-image-update
---
apiVersion: image.toolkit.fluxcd.io/v1beta1
kind: ImagePolicy
metadata:
  name: demo-image
spec:
  imageRepositoryRef:
    name: demo-image
  policy:
    semver:
      range: "v1.x"
---
apiVersion: image.toolkit.fluxcd.io/v1beta1
kind: ImageUpdateAutomation
metadata:
  name: demo-image
spec:
  interval: 1m
  sourceRef:
    kind: GitRepository
    name: demo-image-update
  git:
    checkout:
      ref:
        branch: main
    commit:
      author:
        email: gitopsbot@example.com
        name: gitopsbot
      messageTemplate: |
        [Automated] Image Update

        Automation name: {{ .AutomationObject }}

        Files:
        {{ range $filename, $_ := .Updated.Files -}}
        - {{ $filename }}
        {{ end -}}

        Objects:
        {{ range $resource, $_ := .Updated.Objects -}}
        - {{ $resource.Kind }} {{ $resource.Name }}
        {{ end -}}

        Images:
        {{ range .Updated.Images -}}
        - {{.}}
        {{ end -}}
    push:
      branch: main
  update:
    path: dev
    strategy: Setters
```

After the above resources are set, we can decide which ImagePolicy should be applied to which file and line by specified an anchor.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/team-a-demo-image-update/-/blob/main/dev/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
namespace: team-a-demo-dev
resources:
  - ../base
images:
  - name: docker.io/lokllf/gitops-demo-image-update
    # the comment below is the anchor
    newTag: v1.1.0 # {"$imagepolicy": "gitops-tenant-team-a:demo-image:tag"}
```

## Canary deployment

This projects uses Flagger and Istio to demonstrate canary deployment with GitOps. All the operations are configured by Kubernetes resources. Therefore, no manual operation to Kubernetes cluster has to be done even for complicated deployment methods.

With the following `Canary` resource, when the specified `Deployment` changed, a canary deployment will be triggered. Service for canary deployment will be created by Flagger. After that, canary deployment will be started, and load test will be performed. If the configured expected result is matching the result from metrics, the deployment will be classified as succeed.

```yaml
# ref: https://gitlab.com/lokflam-demo/gitops/team-a-demo-istio-canary/-/blob/main/dev/canary.yaml
apiVersion: flagger.app/v1beta1
kind: Canary
metadata:
  name: podinfo
spec:
  # deployment reference
  targetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: podinfo
  # the maximum time in seconds for the canary deployment
  # to make progress before it is rollback (default 600s)
  progressDeadlineSeconds: 60
  # HPA reference (optional)
  autoscalerRef:
    apiVersion: autoscaling/v2beta2
    kind: HorizontalPodAutoscaler
    name: podinfo
  service:
    # service port number
    port: 9898
    # container port number or name (optional)
    targetPort: 9898
    # Istio gateways (optional)
    gateways:
    - public-gateway.istio-system.svc.cluster.local
    # Istio virtual service host names (optional)
    # hosts:
    # - app.example.com
    # Istio traffic policy (optional)
    trafficPolicy:
      tls:
        # use ISTIO_MUTUAL when mTLS is enabled
        mode: DISABLE
    # Istio retry policy (optional)
    retries:
      attempts: 3
      perTryTimeout: 1s
      retryOn: "gateway-error,connect-failure,refused-stream"
  analysis:
    # schedule interval (default 60s)
    interval: 1m
    # max number of failed metric checks before rollback
    threshold: 5
    # max traffic percentage routed to canary
    # percentage (0-100)
    maxWeight: 50
    # canary increment step
    # percentage (0-100)
    stepWeight: 10
    metrics:
    - name: request-success-rate
      # minimum req success rate (non 5xx responses)
      # percentage (0-100)
      thresholdRange:
        min: 99
      interval: 1m
    - name: request-duration
      # maximum req duration P99
      # milliseconds
      thresholdRange:
        max: 500
      interval: 30s
    # testing (optional)
    webhooks:
      - name: acceptance-test
        type: pre-rollout
        url: http://flagger-loadtester.team-a-demo-dev/
        timeout: 30s
        metadata:
          type: bash
          cmd: "curl -sd 'test' http://podinfo-canary:9898/token | grep token"
      - name: load-test
        url: http://flagger-loadtester.team-a-demo-dev/
        timeout: 5s
        metadata:
          cmd: "hey -z 1m -q 10 -c 2 http://podinfo-canary.team-a-demo-dev:9898/"
```
